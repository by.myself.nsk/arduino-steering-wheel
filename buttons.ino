void setupButtons()
{
    for (byte button = 0; button < buttonCount; button++) {
        lastButtonStates[button] = 0;
    }
}

void processNativeButtons() {
    // за выбор кнопок отвечают пины 0 1 2 3 (активный 0)
    // за чтение кнопок отвечают пины 4 5 6 7

    // здесь будет дополнительная логика со светодиодом:
    // подтягивание к земле группы 1 подтягивает к земле и зеленый светодиод
    // подтягивание к земле группы 3 подтягивает к земле и красный светодиод
    // остается только в нужное время включать питание светодиода

    byte portData;
    byte currentButtonState;
    int currentDPadState;
    byte controllerButtonNumber;
    byte buttonGroupIterator;
    byte buttonPinIterator;
    bool ffbModeLedSet = false;
    
    for (buttonGroupIterator = 0; buttonGroupIterator < 4; buttonGroupIterator++) {
        port1.digitalWrite(buttonGroupIterator, LOW);

//        if (ffbMode == 1 && buttonGroupIterator == 1) {
//          port1.digitalWrite(ffbModeLedPin, HIGH);
//          ffbModeLedSet = true;
//        } else if (ffbMode == 2 && buttonGroupIterator == 3) {
//          port1.digitalWrite(ffbModeLedPin, HIGH);
//          ffbModeLedSet = true;
//        }

        portData = port1.digitalReadAll();

        for (buttonPinIterator = 4; buttonPinIterator < 8; buttonPinIterator++) {
            currentButtonState = (portData & bit(buttonPinIterator)) == 0;

            if (isDPadButtons(buttonGroupIterator)) {
                currentDPadState = getDPadValueByPortData(portData);
//                Serial.print("currentDPadState: ");
//                Serial.print(currentDPadState);
//                Serial.print("\n");
//                delay(1000);

                if (currentDPadState != lastDPadState) {
                    Joystick.setHatSwitch(0, currentDPadState);
                    lastDPadState = currentDPadState;
                }
            } else {
                controllerButtonNumber = getControllerButtonNumberForNativeButtons(buttonGroupIterator, buttonPinIterator);
                byte lastButtonState = lastButtonStates[controllerButtonNumber];
                if (currentButtonState != lastButtonState) {
                    Joystick.setButton(controllerButtonNumber, currentButtonState);
                    lastButtonStates[controllerButtonNumber] = currentButtonState;
                }
            }
        }

        port1.digitalWrite(buttonGroupIterator, HIGH);
//        if (ffbModeLedSet) {
//          port1.digitalWrite(ffbModeLedPin, LOW);
//          ffbModeLedSet = false;
//        }
    }
}

void processAdditionalButtons() {
    for (byte portNumber = 1; portNumber < 3; portNumber++) {
        byte portData;
        if (portNumber == 1) {
            portData = port2.digitalReadAll();
        } else if (portNumber == 2) {
            portData = port3.digitalReadAll();
//        } else if (portNumber == 3) {
//            portData = port4.digitalReadAll();
        }

        byte controllerButtonNumber;
        byte currentButtonState;
       
        for (byte buttonNumber = 0; buttonNumber < 8; buttonNumber++) {
            controllerButtonNumber = getControllerButtonNumberForAdditionalButtons(portNumber, buttonNumber);

            currentButtonState = (portData & bit(buttonNumber)) == 0;
            byte lastButtonState = lastButtonStates[controllerButtonNumber];
            
            if (currentButtonState != lastButtonState) {
             
                Joystick.setButton(controllerButtonNumber, currentButtonState);

                #ifdef useKeyboard
                  if (portNumber == 2 && buttonNumber == 2) {
                    if (currentButtonState) {
                        Keyboard.press(KEY_ESC);
                        digitalWrite(LED_BUILTIN, 1);
                    } else {
                        Keyboard.release(KEY_ESC);
                        digitalWrite(LED_BUILTIN, 0);
                    }
                  }
                #endif
                
                lastButtonStates[controllerButtonNumber] = currentButtonState;
            }
        }
    }
}

byte getControllerButtonNumberForAdditionalButtons(byte portNumber, byte portButtonNumber) {
  // с  DPad 11, без - 15
    const byte nativeButtonCount = 11;
    byte number = portNumber * 8 + portButtonNumber + nativeButtonCount - 8;
    if (portNumber == 3) { // смещаем чуть назад для того чтобы была возможность нормально использовать МКПП (не выходить за пределы 32 кнопок)
      number -= 3;
    }
    return number;
}

bool isDPadButtons(byte buttonGroupIterator)
{
    return buttonGroupIterator == 0;
}

// отдаст int-значение - -1 или градусы
int getDPadValueByPortData(byte portData)
{
    bool up = (portData & bit(4)) == 0;
    bool right = (portData & bit(5)) == 0;
    bool down = (portData & bit(6)) == 0;
    bool left = (portData & bit(7)) == 0;

    return getDPadValueByButtonStates(up, right, down, left);
}

int getDPadValueByButtonStates(bool up, bool right, bool down, bool left)
{
    if (!up && !right && !down && !left) {
        return -1;
    }
    if (up) {
        if (left) {
            return 315;
        }
        if (right) {
            return 45;
        }
        return 0;
    }
    if (down) {
        if (left) {
            return 225;
        }
        if (right) {
            return 135;
        }
        return 180;
    }
    if (left) {
        return 270;
    }
    if (right) {
        return 90;
    }
    return -1;
}


byte getControllerButtonNumberForNativeButtons(byte buttonGroupIterator, byte buttonPinIterator) {
    // вернуть согласно маппингу
    /*
    тут будем писать маппинг портов и пинов к кнопкам контроллера
    адрес   номер   номер
    порта   пина    кнопки
            кнопки  контроллера
    0       4       DU
    0       5       DR
    0       6       DD
    0       7       DL
    1       4       2
    1       5       3
    1       6       9 (SE)
    1       6       10 (ST)
    2       4       1
    2       5       7
    2       6       8
    2       7       4
    3       4       11 (F)
    3       5       5
    3       6       6
    3       7
    */
//    byte group1Mapping[] = {2, 3, 9, 10};
//    byte group2Mapping[] = {1, 7, 8, 4};
//    byte group3Mapping[] = {5, 6, 11, 255};
    byte groups[][4] = {
        {255, 255, 255, 255}, // DPad, обрабатывается отдельно
        {2, 3, 9, 10},
        {1, 7, 8, 4},
        {11, 5, 6, 255},
    };
    // buttonPinIterator меняется от 4 до 7
    byte pos = buttonPinIterator - 4;
    return groups[buttonGroupIterator][pos] - 1;
}
