#include "Joystick.h"
#include "PCF8574.h"
#include "PCF8575.h"
//#include "Keyboard.h"
#define joystickWithFFb true
//#define useKeyboard true

#define steeringPin A1
#define steeringPinNumber 1 // для получения значений для пружины
#define acceleratorPin A2
#define brakePin A3
#define clutchPin A4
#define stick1Y A11
#define stick1X A9
#define stick2Y A10
#define stick2X A8
#define stick2YIteratorNumber 11
#define stick2XIteratorNumber 9

#define ffbModeButtonPin 5
#define ffbModeLedPin 0
#define motorPinA 11
#define motorPinB 13
#define motorPinPwm 7

//#define emptyAnalog1 A0
//#define emptyAnalog2 A5
//#define emptyAnalog3 A6
//#define emptyAnalog4 A7

const bool autoSendMode = false;       // режим отправки состояния контроллера
const byte buttonCount = 40;           // количество кнопок
const byte analogCount = 12;            // количество аналоговых осей

byte lastButtonStates[buttonCount];    // массив последних состояний кнопок игрового контроллера
int lastDPadState = -1;
const byte averageAnalogArraySize = 8; // размер массива для усреднения данных аналоговых осей
byte averageAnalogIterator = 0;        //инкрементный итератор для прохода массива средних значений. пока не определено, считаем первым шагом, заполняя весь массив одним значением
bool canSendAnalogData = false;        // указатель на позицию сделал один "круг", можно слать усредненные данные на ПК
unsigned int lastAnalogStates[analogCount][averageAnalogArraySize]; // массив средних значений аналоговых портов
unsigned int analogRanges[analogCount][2]; // границы аналоговых осей (мин и макс значения)

const byte maxForce = 128;
#ifdef joystickWithFFb
Gains mygains[2];
EffectParams myeffectparams[2];
#endif
int32_t forces[2] = {0}; // тип не менять, т.к. передается по ссылке в метод
unsigned int valueForFeedback;

const byte ffbModesCount = 3;
byte ffbMode = 3; // 1 2 3
bool ffbModeButtonLastState = 0;

const bool simpleAxis = true;
String analogPortMapping[] = { // analogCount
        "",
        simpleAxis ? "X" : "Steering",
        "Z",           // сцепление
        simpleAxis ? "Rz" : "Brake",
        simpleAxis ? "Y" : "Accelerator",
        "", // A5
        "", // A6
        "", // A7
        "HatSwitch2X", // стик 2 X
        "Rx",          // стик 1 X
        "HatSwitch2Y", // стик 2 Y
        "Ry",          // стик 1 Y
};

//#define stick1Y A11
//#define stick1X A9
//#define stick2Y A10
//#define stick2X A8
//#define stick2YIteratorNumber 11
//#define stick2XIteratorNumber 9



// для библиотеки FFB почему-то принципиально иметь адрес 0x01
Joystick_ Joystick(
        JOYSTICK_DEFAULT_REPORT_ID,//JOYSTICK_DEFAULT_REPORT_ID / 0x01 / 0x03 / 0x8B
        JOYSTICK_TYPE_JOYSTICK,//JOYSTICK_TYPE_MULTI_AXIS,
        buttonCount,
        2,           // hat switch
        simpleAxis,  // X Axis
        simpleAxis,  // Y Axis
        true,        // Z Axis
        true,        // Rx Axis
        true,        // Ry Axis
        simpleAxis,  // Rz Axis
        false,       // Rudder
        false,       // Throttle
        !simpleAxis, // accelerator
        !simpleAxis, // brake
        !simpleAxis  // steering
);

PCF8574 port1(0x20);
PCF8574 port2(0x21);
PCF8574 port3(0x22);
//PCF8574 port4(0x23);
PCF8575 portGearbox(0x27);

void setup() {
//    setupSerial();
    setupArduinoPorts();
    setupAdditionalPorts();
    setupGearboxPorts();
    setupButtons();
    setupFeedback();

    setupJoystick();

#ifdef useKeyboard
    Keyboard.begin();
#endif
//    digitalWrite(LED_BUILTIN, 1);
//  Keyboard.press(KEY_ESC);
//  delay(500);
//  Keyboard.release(KEY_ESC);
}

void loop() {
//  digitalWrite(LED_BUILTIN, 1);
//    processFfbButton();
    processNativeButtons();
    processAdditionalButtons();
    processGearboxButtons();
    processAnalog(true);

    processJoystick(); // на всякий случай шлём данные в ПК - в примере с feedback forces обрабатываются после Joystick.setXAxis(value);

    processFeedback();

    processJoystick();
}
