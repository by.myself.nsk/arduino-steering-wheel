void processAnalog(bool needSendData) {
    unsigned int readValue;
    unsigned int averageValue;
    for (byte analogPort = 0; analogPort < analogCount; analogPort++) {
        readValue = analogRead(analogPort);

        rememberReadValue(analogPort, readValue);
        if (needSendData && canSendAnalogData) {
            averageValue = getAverageValue(analogPort);
            checkAnalogRange(analogPort, averageValue);
            setAnalogValue(analogPort, averageValue);
        }
    }

    averageAnalogIterator++;
    if (averageAnalogIterator == averageAnalogArraySize) {
        averageAnalogIterator = 0;
        canSendAnalogData = true;
    }

    if (needSendData && canSendAnalogData) {
        processHatSwitch2();
    }
}

void rememberReadValue(byte analogPort, unsigned int value) {
    lastAnalogStates[analogPort][averageAnalogIterator] = value;
}

unsigned int getAverageValue(byte analogPort) {
    unsigned int sum = 0;
    for (byte i = 0; i < averageAnalogArraySize; i++) {
        sum += lastAnalogStates[analogPort][i];
    }
    return sum / averageAnalogArraySize;
}

void checkAnalogRange(byte analogPort, unsigned int value) {
//    const byte minRangePosition = 0;
//    const byte maxRangePosition = 1;

    bool needUpdate = false;
    if (analogRanges[analogPort][0] == NULL) {
        analogRanges[analogPort][0] = value;
    } else {
        if (analogRanges[analogPort][0] > value) {
            analogRanges[analogPort][0] = value;
            needUpdate = true;
        }
    }
    if (analogRanges[analogPort][1] == NULL) {
        analogRanges[analogPort][1] = value;
    } else {
        if (analogRanges[analogPort][1] < value) {
            analogRanges[analogPort][1] = value;
            needUpdate = true;
        }
    }
    if (needUpdate) {
        setAnalogRange(analogPort, analogRanges[analogPort][0], analogRanges[analogPort][1]);
        needUpdate = false;
    }
}

void setAnalogRange(byte analogPort, unsigned int min, unsigned int max) {
    if (analogPortMapping[analogPort] == "X") {
        Joystick.setXAxisRange(min, max);
    } else if (analogPortMapping[analogPort] == "Y") {
        Joystick.setYAxisRange(min, max);
    } else if (analogPortMapping[analogPort] == "Z") {
        Joystick.setZAxisRange(min, max);
    } else if (analogPortMapping[analogPort] == "Rx") {
        Joystick.setRxAxisRange(min, max);
    } else if (analogPortMapping[analogPort] == "Ry") {
        Joystick.setRyAxisRange(min, max);
    } else if (analogPortMapping[analogPort] == "Rz") {
        Joystick.setRzAxisRange(min, max);
    } else if (analogPortMapping[analogPort] == "Rudder") {
        Joystick.setRudderRange(min, max);
    } else if (analogPortMapping[analogPort] == "Throttle") {
        Joystick.setThrottleRange(min, max);
    } else if (analogPortMapping[analogPort] == "Accelerator") {
        Joystick.setAcceleratorRange(min, max);
    } else if (analogPortMapping[analogPort] == "Brake") {
        Joystick.setBrakeRange(min, max);
    } else if (analogPortMapping[analogPort] == "Steering") {
        Joystick.setSteeringRange(min, max);
    }
}

void setAnalogValue(byte analogPort, unsigned int value) {
    if (analogPortMapping[analogPort] == "X") {
        Joystick.setXAxis(value);
        if (simpleAxis) {
            valueForFeedback = value;
        }
    } else if (analogPortMapping[analogPort] == "Y") {
        Joystick.setYAxis(value);
    } else if (analogPortMapping[analogPort] == "Z") {
        Joystick.setZAxis(value);
    } else if (analogPortMapping[analogPort] == "Rx") {
        Joystick.setRxAxis(value);
    } else if (analogPortMapping[analogPort] == "Ry") {
        Joystick.setRyAxis(value);
    } else if (analogPortMapping[analogPort] == "Rz") {
        Joystick.setRzAxis(value);
    } else if (analogPortMapping[analogPort] == "Rudder") {
        Joystick.setRudder(value);
    } else if (analogPortMapping[analogPort] == "Throttle") {
        Joystick.setThrottle(value);
    } else if (analogPortMapping[analogPort] == "Accelerator") {
        Joystick.setAccelerator(value);
    } else if (analogPortMapping[analogPort] == "Brake") {
        Joystick.setBrake(value);
    } else if (analogPortMapping[analogPort] == "Steering") {
        Joystick.setSteering(value);
        if (!simpleAxis) {
            valueForFeedback = value;
        }
    }
}


byte lastButtonStatesStick2[4];    // массив последних состояний кнопок Stick2

void processHatSwitch2() {
    // взять значение порта X
    // взять значение порта Y
    //stick2X;
    //stick2Y;
    //getAverageValue

    const byte toRange = 6; // определяет то, какое отклонение стика даст нажатие. чем меньше значение, тем сильнее надо отклонить стик

    unsigned int stick2XVal = getAverageValue(stick2XIteratorNumber);
    unsigned int stick2YVal = getAverageValue(stick2YIteratorNumber);

    int xVal = map(stick2XVal, analogRanges[stick2XIteratorNumber][0], analogRanges[stick2XIteratorNumber][1], -toRange, toRange);
    int yVal = map(stick2YVal, analogRanges[stick2YIteratorNumber][0], analogRanges[stick2YIteratorNumber][1], -toRange, toRange);

//    Serial.print("stick2XVal: ");
//    Serial.print(stick2XVal);
//    Serial.print("\n");
//    Serial.print("xVal: ");
//    Serial.print(xVal);
//    Serial.print("\n");
//    delay(250);
  
    bool up = xVal < -1;
    bool right = yVal < -1;
    bool down = xVal > 1;
    bool left = yVal > 1;

//    int dPadValue = getDPadValueByButtonStates(up, right, down, left);
//    Joystick.setHatSwitch(1, dPadValue);

//----------------------------------------------------------------------
  // byte analogToButtonMapping[4]= {28, 29, 30, 31};
  byte analogToButtonMapping[4]= {36, 37, 38, 39};

  if (up != lastButtonStatesStick2[0]) {
    lastButtonStatesStick2[0] = up;
    Joystick.setButton(analogToButtonMapping[0], up);
  }
  if (right != lastButtonStatesStick2[1]) {
    lastButtonStatesStick2[1] = right;
    Joystick.setButton(analogToButtonMapping[1], right);
  }
  if (down != lastButtonStatesStick2[2]) {
    lastButtonStatesStick2[2] = down;
    Joystick.setButton(analogToButtonMapping[2], down);
  }
  if (left != lastButtonStatesStick2[3]) {
    lastButtonStatesStick2[3] = left;
    Joystick.setButton(analogToButtonMapping[3], left);
  }
//----------------------------------------------------------------------
  
}
