void setFeedbackData(int force)
{
    force = constrain(force, -maxForce, maxForce);
    bool outX1 = force > 0;
    bool outX2 = force < 0;
    byte pwm = abs(force);

    digitalWrite(motorPinA, outX1);
    digitalWrite(motorPinB, outX2);
    analogWrite(motorPinPwm, pwm);
}

void setupFeedback()
{
#ifdef joystickWithFFb
    mygains[0].totalGain = 100;//0-100
    mygains[0].springGain = 100;//0-100
    //enable gains REQUIRED
    Joystick.setGains(mygains);
//    myeffectparams[0].springMaxPosition = 1023; в примере это в loop
#endif
    initFeedbackRanges();
}

void initFeedbackRanges()
{
    setFeedbackData(maxForce);
    delay(1500);
    for(byte initAnalogIterator = 0; initAnalogIterator < averageAnalogArraySize; initAnalogIterator++) {
        processAnalog(false);
    }
    processAnalog(true);

    setFeedbackData(-maxForce);
    delay(1500);
    for(byte initAnalogIterator = 0; initAnalogIterator < averageAnalogArraySize; initAnalogIterator++) {
        processAnalog(false);
    }
    processAnalog(true);

    setFeedbackData(0);
}

void processFeedback()
{
#ifdef joystickWithFFb
    myeffectparams[0].springMaxPosition = 1023; // непонятно, зачем, но на всякий случай повторим с примера
    myeffectparams[0].springPosition = valueForFeedback;
    Joystick.setEffectParams(myeffectparams); // Recv HID-PID data from PC and caculate forces
#endif
    int forceX = getFfbForce();
    setFeedbackData(forceX);
}

void processFfbButton()
{
  bool pressed = !digitalRead(ffbModeButtonPin);
  if (!ffbModeButtonLastState && pressed) {
      ffbMode = ffbMode == ffbModesCount ? 1 : ffbMode + 1;
  }
  if (ffbModeButtonLastState != pressed) {
      ffbModeButtonLastState = pressed;
  }
}

int32_t getFfbForce() {
    // должно выполняться всегда
    Joystick.getForce(forces);
    switch(ffbMode){
        case 1: // free
            return 0;
            break;
        case 2: // spring
//        return 100;
            // ex: [7, 997], val = 321
            // force should be [-255, 255]
            unsigned int minRange = analogRanges[steeringPinNumber][0];
            unsigned int maxRange = analogRanges[steeringPinNumber][1];
            unsigned int rangeAbs = maxRange - minRange;
            if (!rangeAbs) {
                return 0;
            }
            unsigned int midAbs = rangeAbs / 2;
            int t = valueForFeedback - midAbs - minRange;
            float k = maxForce * 1.0 / midAbs;
            int force = - t * k;
            // на данный момент уже посчитан force пропорционально от -255 до 255, с 0 в центре руля.
            // При этом, ближе к нулю "пружина" "слабая", и force можно немного скорректировать коэффициентом
            force = force * 3.0;
            return constrain(force, -maxForce, maxForce);
            break;
        case 3: // feedback
            #ifdef joystickWithFFb

//            Joystick.getForce(forces);
            return forces[0];
            
            #else

            return 0;
            
            #endif
            
            break;
    }
}
