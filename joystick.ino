void setupJoystick()
{
    if (autoSendMode) {
        Joystick.begin();
    } else {
        Joystick.begin(false);
    }
}

void processJoystick() {
    if (autoSendMode == false) {
        Joystick.sendState();
    }
}
