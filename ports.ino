void setupArduinoPorts()
{
    for(byte pin = 0; pin < 14; pin++) {
        pinMode(pin, OUTPUT);
    }
  
    pinMode(steeringPin, INPUT);
    pinMode(acceleratorPin, INPUT);
    pinMode(brakePin, INPUT);
    pinMode(clutchPin, INPUT);
    pinMode(stick1X, INPUT);
    pinMode(stick1Y, INPUT);
    pinMode(stick2X, INPUT);
    pinMode(stick2Y, INPUT);
    pinMode(ffbModeButtonPin, INPUT_PULLUP);
    pinMode(ffbModeLedPin, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(motorPinA, OUTPUT);
    pinMode(motorPinB, OUTPUT);
    pinMode(motorPinPwm, OUTPUT);
}

void setupAdditionalPorts()
{
    port1.pinMode(0, OUTPUT);
    port1.pinMode(1, OUTPUT);
    port1.pinMode(2, OUTPUT);
    port1.pinMode(3, OUTPUT);
    port1.pinMode(4, INPUT);
    port1.pinMode(5, INPUT);
    port1.pinMode(6, INPUT);
    port1.pinMode(7, INPUT);

    port1.digitalWrite(0, HIGH);
    port1.digitalWrite(1, HIGH);
    port1.digitalWrite(2, HIGH);
    port1.digitalWrite(3, HIGH);

    for (uint8_t p = 0; p < 8; p++) {
        port2.pinMode(p, INPUT);
        port3.pinMode(p, INPUT);
//        port4.pinMode(p, INPUT);
    }
    port1.begin();
    port2.begin();
    port3.begin();
//    port4.begin();
}
