void setupGearboxPorts()
{
  // видимо, в этой библиотеке этого не требуется
//    for (uint8_t p = 0; p < 16; p++) {
//        portGearbox.pinMode(p, INPUT);
//    }
    portGearbox.begin();
}

void processGearboxButtons() {
    uint16_t portData = portGearbox.read16();

    byte controllerButtonNumber;
    byte currentButtonState;
    for (byte buttonNumber = 0; buttonNumber < 16; buttonNumber++) {
        controllerButtonNumber = getControllerButtonNumberForGearboxButtons(buttonNumber);
        currentButtonState = (portData & bit(buttonNumber)) == 0;
        byte lastButtonState = lastButtonStates[controllerButtonNumber];
        if (currentButtonState != lastButtonState) {
            Joystick.setButton(controllerButtonNumber, currentButtonState);
            lastButtonStates[controllerButtonNumber] = currentButtonState;
        }
    }
}

byte getControllerButtonNumberForGearboxButtons(byte portButtonNumber) {
    byte number = portButtonNumber + 22;
    return number;
}
